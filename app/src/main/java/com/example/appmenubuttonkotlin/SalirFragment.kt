package com.example.appmenubuttonkotlin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.appmenubuttonkotlin.database.Alumno
import com.example.appmenubuttonkotlin.database.dbAlumnos
import com.google.android.material.floatingactionbutton.FloatingActionButton


class SalirFragment : Fragment() {

    private lateinit var rcyLista : RecyclerView
    private lateinit var adapter : MiAdaptador
    private lateinit var btnNuevo : FloatingActionButton
    val listaAlumno = ArrayList<AlumnoLista>()
    private lateinit var db : dbAlumnos
    private lateinit var searchView: SearchView



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_salir, container, false)

        // Inflate the layout for this fragment
        db = dbAlumnos(requireContext())
        db.openDataBase()

        rcyLista = view.findViewById(R.id.recId)
        btnNuevo = view.findViewById(R.id.agregarAlumno)
        rcyLista.layoutManager =  LinearLayoutManager(requireContext())
        adapter = MiAdaptador(db.leerTodosAlumnoLista(), requireContext())

        rcyLista.adapter = adapter



        cargaAlumnos()
        db.close()


        btnNuevo.setOnClickListener{
            cambiarDbFragment()
        }

        adapter.setOnClickListener({
            val pos : Int =  rcyLista.getChildAdapterPosition(it)

            val alumno: AlumnoLista
            alumno = listaAlumno[pos]

            val bundle =  Bundle().apply {
                putSerializable("mialumno", alumno)
            }

            val dbFragment = dbFragment()
            dbFragment.arguments = bundle

            parentFragmentManager.beginTransaction()
                .replace(R.id.frmContenedor, dbFragment)
                .addToBackStack(null)
                .commit()
        })

        return view

    }

    private fun cambiarDbFragment() {
        val cambioFragment = dbFragment()
        cambioFragment.setTargetFragment(this, 1)
        val transaction = fragmentManager?.beginTransaction()
        transaction?.replace(R.id.frmContenedor, cambioFragment)
        transaction?.addToBackStack(null)
        transaction?.commit()
    }

    private fun cargaAlumnos() {
        try {
            val alumnos = db.leerTodos()
            listaAlumno.clear()
            for (alumno in alumnos) {
                val alumnoLista = AlumnoLista(
                    id = alumno.id,
                    matricula = alumno.matricula,
                    nombre = alumno.nombre,
                    domicilio = alumno.domicilio,
                    especialidad = alumno.especialidad,
                    foto = alumno.foto
                )
                listaAlumno.add(alumnoLista)
            }
            adapter.notifyDataSetChanged()
        } catch (e: Exception) {
            Toast.makeText(requireContext(), "Error al cargar los alumnos", Toast.LENGTH_SHORT).show()
        }
    }

     fun onItemClick(alumno: AlumnoLista) {
        val fragment = dbFragment()
        val bundle = Bundle()
        bundle.putSerializable("mialumno", alumno)
        fragment.arguments = bundle

        fragmentManager?.beginTransaction()
            ?.replace(R.id.frmContenedor, fragment)
            ?.addToBackStack(null)
            ?.commit()
    }

    fun onAlumnoAdded(alumno: Alumno) {
        val alumnoLista = AlumnoLista(
            id = alumno.id,
            matricula = alumno.matricula,
            nombre = alumno.nombre,
            domicilio = alumno.domicilio,
            especialidad = alumno.especialidad,
            foto = alumno.foto
        )
        listaAlumno.add(alumnoLista)
        adapter.notifyItemInserted(listaAlumno.size - 1)
    }






}
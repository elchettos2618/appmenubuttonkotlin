package com.example.appmenubuttonkotlin

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.example.appmenubuttonkotlin.database.Alumno
import com.example.appmenubuttonkotlin.database.dbAlumnos
import com.squareup.picasso.Picasso
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class dbFragment : Fragment() {

    private lateinit var db: dbAlumnos
    private lateinit var btnAgrega: Button
    private lateinit var btnBuscar: Button
    private lateinit var btnEliminar: Button
    private lateinit var txtMatricula: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtDomicilio: EditText
    private lateinit var txtUrlImagen: EditText
    private lateinit var txtEspecialidad: EditText
    private lateinit var imgAlumno: ImageView

    private val IMAGE_PICK_CODE = 1000


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_db, container, false)

        // Inicializar la base de datos primero
        db = dbAlumnos(requireContext())
        db.openDataBase()

        iniciarComponentes(view)

        btnAgrega.setOnClickListener {
            if (txtMatricula.text.toString().isEmpty() ||
                txtNombre.text.toString().isEmpty() ||
                txtDomicilio.text.toString().isEmpty() ||
                txtEspecialidad.text.toString().isEmpty()) {
                Toast.makeText(context, "Debe llenar todos los datos", Toast.LENGTH_SHORT).show()
            } else {
                val fotoPath = txtUrlImagen.text.toString()
                val alumno = Alumno(
                    nombre = txtNombre.text.toString(),
                    matricula = txtMatricula.text.toString(),
                    domicilio = txtDomicilio.text.toString(),
                    especialidad = txtEspecialidad.text.toString(),
                    foto = fotoPath
                )

                val query = db.getAlumno(alumno.matricula)

                if (query.id != 0) {
                    db.ActualizarAlumno(alumno, alumno.matricula)
                    txtMatricula.text.clear()
                    txtNombre.text.clear()
                    txtDomicilio.text.clear()
                    txtEspecialidad.text.clear()
                    Toast.makeText(requireContext(), "Se actualizó el alumno", Toast.LENGTH_SHORT).show()
                } else {
                    val id: Long = db.InsertarAlumno(alumno)
                    txtMatricula.text.clear()
                    txtNombre.text.clear()
                    txtDomicilio.text.clear()
                    txtEspecialidad.text.clear()
                    Toast.makeText(requireContext(), "Se agregó el alumno", Toast.LENGTH_SHORT).show()
                }
            }
        }

        btnBuscar.setOnClickListener {
            if (txtMatricula.text.isEmpty()) {
                Toast.makeText(context, "Ingresar matrícula a buscar", Toast.LENGTH_SHORT).show()
            } else {
                db.openDataBase()
                val alumno: Alumno = db.getAlumno(txtMatricula.text.toString())
                if (alumno.id != 0) {
                    txtNombre.setText(alumno.nombre)
                    txtDomicilio.setText(alumno.domicilio)
                    txtEspecialidad.setText(alumno.especialidad)
                    loadImage(alumno.foto)
                    Toast.makeText(context, "Alumno encontrado", Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(context, "No se encontró el alumno", Toast.LENGTH_SHORT).show()
                }
            }
        }

        imgAlumno.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, IMAGE_PICK_CODE)
        }

        btnEliminar.setOnClickListener {
            if (txtMatricula.text.isEmpty()) {
                Toast.makeText(context, "Ingresar matrícula a buscar", Toast.LENGTH_SHORT).show()
            } else {
                val alumno = db.getAlumno(txtMatricula.text.toString())

                if (alumno.id != 0) {
                    val builder = AlertDialog.Builder(requireContext())
                    builder.setTitle("Lista de Alumnos")
                    builder.setMessage("¿Está seguro de borrar el alumno?")
                    builder.setNegativeButton("No") { dialog, _ ->
                        dialog.dismiss()
                    }
                    builder.setPositiveButton("Sí") { _, _ ->
                        db.BorrarAlumno(alumno.matricula)
                        Toast.makeText(context, "Alumno borrado", Toast.LENGTH_SHORT).show()

                        // Limpiar campos después de borrar
                        txtMatricula.text.clear()
                        txtNombre.text.clear()
                        txtDomicilio.text.clear()
                        txtEspecialidad.text.clear()
                        imgAlumno.setImageResource(0)
                        txtUrlImagen.text.clear()

                    }

                    builder.show()
                } else {
                    Toast.makeText(context, "No existe el alumno", Toast.LENGTH_SHORT).show()
                }
            }
        }

        return view
    }

    private fun iniciarComponentes(view: View) {
        btnAgrega = view.findViewById(R.id.btnAgregar)
        btnBuscar = view.findViewById(R.id.btnBuscar)
        btnEliminar = view.findViewById(R.id.btnBorrar)
        txtMatricula = view.findViewById(R.id.txtMatricula)
        txtNombre = view.findViewById(R.id.txtNombre)
        txtDomicilio = view.findViewById(R.id.txtDomicilio)
        txtEspecialidad = view.findViewById(R.id.txtEspecialidad)
        txtUrlImagen = view.findViewById(R.id.txtURLFoto)
        imgAlumno = view.findViewById(R.id.imgFoto)

        arguments?.let {
            val alumnoLista = it.getSerializable("mialumno") as? AlumnoLista
            alumnoLista?.let { alumno ->
                txtNombre.setText(alumno.nombre)
                txtMatricula.setText(alumno.matricula)
                txtDomicilio.setText(alumno.domicilio)
                txtEspecialidad.setText(alumno.especialidad)
                txtUrlImagen.setText(alumno.foto)
                loadImage(alumno.foto)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            val imageUri = data?.data
            imageUri?.let {
                try {
                    val savedImagePath = saveImageToInternalStorage(it)
                    Picasso.get().load(it).into(imgAlumno)
                    txtUrlImagen.setText(savedImagePath)
                } catch (e: IOException) {
                    Toast.makeText(requireContext(), "Error al guardar la imagen", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun saveImageToInternalStorage(uri: Uri): String {
        val inputStream = requireContext().contentResolver.openInputStream(uri)
        val file = File(requireContext().filesDir, "${System.currentTimeMillis()}.jpg")
        val outputStream = FileOutputStream(file)
        inputStream.use { input ->
            outputStream.use { output ->
                input?.copyTo(output)
            }
        }
        return file.absolutePath
    }

    private fun loadImage(imagePath: String?) {
        if (imagePath != null) {
            val imageFile = File(imagePath)
            if (imageFile.exists()) {
                Picasso.get().load(imageFile).into(imgAlumno)
            } else {
                imgAlumno.setImageResource(R.mipmap.ic_launcher)
            }
        } else {
            imgAlumno.setImageResource(R.mipmap.ic_launcher)
        }
    }
}

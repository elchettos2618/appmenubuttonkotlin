package com.example.appmenubuttonkotlin

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.view.MenuItemCompat
import androidx.core.view.get
import androidx.recyclerview.widget.RecyclerView


class ListFragment : Fragment() {
    // TODO: Rename and change types of parameters

    lateinit var listView: ListView
    lateinit var arrayList: ArrayList<String>
    lateinit var adapter: ArrayAdapter<String>
    lateinit var searchView: SearchView


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)
        // Inflate the layout for this fragment
        listView = view.findViewById(R.id.lstAlumnos)


        val items = resources.getStringArray(R.array.alumnos)
        searchView = view.findViewById(R.id.svBuscador)

        arrayList = java.util.ArrayList()
        arrayList.addAll(items)

        adapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, arrayList)

        listView.adapter = adapter

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (arrayList.contains(query)){
                    adapter.filter.filter(query)

                } else{
                    Toast.makeText(activity, "Alumno Inexistente", Toast.LENGTH_SHORT).show()
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {

                adapter.filter.filter(newText)
                return true
            }

        })

        listView.setOnItemClickListener(AdapterView.OnItemClickListener { parent, view, position, id ->
            var alumno:String = parent.getItemAtPosition(position).toString()
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Lista de Alumnos")
            builder.setMessage(position.toString() + ": " + alumno)
            builder.setPositiveButton("ok"){dialog, which ->

            }
            builder.show()
        })

        return view
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.searchmenu, menu)

        val item = menu?.findItem(R.id.svBuscador);
        val searchView = item?.actionView as SearchView

        // search queryTextChange Listener
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(query: String?): Boolean {
                Log.d("onQueryTextChange", "query: " + query)
                return true
            }
        })

        //Expand Collapse listener
        item.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                showToast("Action Collapse")
                return true
            }

            override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                showToast("Action Expand")
                return true
            }
        })
        return super.onCreateOptionsMenu(menu, inflater )
    }

    private fun showToast(msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
    }


}
